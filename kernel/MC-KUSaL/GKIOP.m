function [ Dinit, subidxinit, Nlinit ] = GKIOP( centergram, L, s )

N = size(centergram,2);
subidxinit = zeros(L,N);  % the i-th row contains the indexes of signals belong to i-th subspace
Nlinit = ones(L,1)*s;    % number of signals belong to each subspace
Dinit = zeros(N,s,L);  % bases representation matrices

signaln = N;
signalidx = 1:N;

for i = 1:L
    
    subidxinit(i,1) = signalidx(randperm(signaln,1));
    signalidx = setdiff(signalidx,subidxinit(i,1));
    signaln = signaln - 1;
    
    for j = 2:s
        tempG = centergram(signalidx,subidxinit(i,1:j-1));
        tempsum = sum(tempG,2);
        [~,tempidx] = max(tempsum);
        subidxinit(i,j) = signalidx(tempidx);
        signalidx = setdiff(signalidx,subidxinit(i,j));
        signaln = signaln - 1;
    end
    
    Gz = centergram(subidxinit(i,1:s),subidxinit(i,1:s));
    [V,sigma] = eig(Gz);
    sigma = diag(sigma);
    [sigma,IX] = sort(sigma,'descend');
    V = V(:,IX);
    sigma = diag(sigma);
    Dinit(1:s,:,i) = V*(sigma^(-1/2));
    % Dict(1:dim,:,i)'*Gz*Dict(1:dim,:,i)
    clear Gz V sigma IX
end

end

