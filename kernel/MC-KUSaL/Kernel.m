function k = Kernel(x, Y, kernel, param1, param2)

% Calculate the kernel function between a point x and a matrix Y, returns a
% 1 x N matrix, where N = size(Y,2)

    % Check inputs
    if size(x,1) ~= size(Y,1)
        error('Dimensionality of both datasets should be equal');
    end

    % If no kernel function is specified
    if nargin == 2 || strcmp(kernel, 'none')
        kernel = 'linear';
    end
    
    switch kernel
        
        % Linear kernel
        case 'linear'
            k = x' * Y;
        
        % Gaussian kernel
        case 'gauss'
            if ~exist('param1', 'var'), param1 = 1; end
            k = sum( (repmat(x,1,size(Y,2)) - Y ).^2 );   
            k = exp(-(k./param1));
                        
        % Polynomial kernel
        case 'poly'
            if ~exist('param1', 'var'), param1 = 1; param2 = 3; end
            k = (x'*Y + param1).^param2;
            
        otherwise
            error('Unknown kernel function.');
    end
end