function prex = GaussianMCKUoS_PreImage(grammatrix, train, Dtau, signalidx, input, param1)

N = size(grammatrix,2);
Ntau = length(signalidx);
k_x = Kernel(input, train, 'gauss', param1)';
psi = k_x(signalidx);

aa = 1/N*grammatrix(signalidx,:)*ones(N,1);
bb = 1/(N^2)*ones(Ntau,1)*ones(1,N)*grammatrix*ones(N,1);
psi_tilde = psi - 1/N*ones(Ntau,1)*ones(1,N)*k_x - aa + bb;

dist2 = zeros(N,1);
for i = 1:N
    pp = psi_tilde + 2*aa - 2*bb - 2*grammatrix(signalidx,i) + 2/N*ones(Ntau,1)*ones(1,N)*grammatrix(:,i);
    dist2(i) = psi_tilde'*(Dtau*Dtau')*pp + 1 + 1/(N^2)*ones(1,N)*grammatrix*ones(N,1) - 2/N*ones(1,N)*grammatrix(:,i);
    clear pp
end

gamma = zeros(1,N);
gamma(signalidx) = psi_tilde'*(Dtau*Dtau');
gamma = gamma + 1/N*ones(1,N) - 1/N*psi_tilde'*(Dtau*Dtau')*ones(Ntau,1)*ones(1,N);
coeff = gamma'.*(1- 0.5*dist2);

prex = train*coeff/sum(coeff);

end