function prex = MissGaussianMCKUoS_PreImage(missgram, misstrain, Dtau, signalidx, missinput, param1)

N = size(missgram,2);
Ntau = length(signalidx);
k_x = Appro_kernel(missinput, misstrain, 'gauss', param1)';
psi = k_x(signalidx);

aa = 1/N*missgram(signalidx,:)*ones(N,1);
bb = 1/(N^2)*ones(Ntau,1)*ones(1,N)*missgram*ones(N,1);
psi_tilde = psi - 1/N*ones(Ntau,1)*ones(1,N)*k_x - aa + bb;

dist2 = zeros(N,1);
for i = 1:N
    pp = psi_tilde + 2*aa - 2*bb - 2*missgram(signalidx,i) + 2/N*ones(Ntau,1)*ones(1,N)*missgram(:,i);
    dist2(i) = psi_tilde'*(Dtau*Dtau')*pp + missgram(i,i) + 1/(N^2)*ones(1,N)*missgram*ones(N,1) - 2/N*ones(1,N)*missgram(:,i);
    clear pp
end


gamma = zeros(1,N);
gamma(signalidx) = psi_tilde'*(Dtau*Dtau');
gamma = gamma + 1/N*ones(1,N) - 1/N*psi_tilde'*(Dtau*Dtau')*ones(Ntau,1)*ones(1,N);
coeff = gamma'.*(1- 0.5*dist2);

prex = zeros(size(missinput,1),1);
for i = 1:length(prex)
    idx = find(~isnan(misstrain(i,:)));
    prex(i) = misstrain(i,idx)*coeff(idx)/(sum(coeff)/N*length(idx));
    clear idx
end

end