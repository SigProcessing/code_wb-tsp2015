function k = Appro_kernel(missx, missY, kernel, param1, param2)

% Calculate the kernel function between a point x and a matrix Y, returns a
% 1 x N matrix, where N = size(Y,2)

    % Check inputs
    if size(missx,1) ~= size(missY,1)
        error('Dimensionality of both datasets should be equal');
    end

    % If no kernel function is specified
    if nargin == 2 || strcmp(kernel, 'none')
        kernel = 'linear';
    end
    
    n = length(missx);
    N = size(missY,2);
    k = zeros(1,N);
    switch kernel
        
        % Linear kernel
        case 'linear'
            for i = 1:N
                innerprod = missx.*missY(:,i);
                idx = find(~isnan(innerprod));
                m = length(idx);
                k(i) = n/m*sum(innerprod(idx));
                clear innerprod idx m
            end
            
        % Gaussian kernel
        case 'gauss'
            if ~exist('param1', 'var'), param1 = 1; end
            for i = 1:N
                minus = missx-missY(:,i);
                idx = find(~isnan(minus));
                m = length(idx);
                k(i) = n/m*sum(minus(idx).^2);
                clear minus idx m
            end
            k = exp(-(k./param1));
                        
        % Polynomial kernel
        case 'poly'
            if ~exist('param1', 'var'), param1 = 1; param2 = 3; end
            for i = 1:N
                innerprod = missx.*missY(:,i);
                idx = find(~isnan(innerprod));
                m = length(idx);
                k(i) = ( n/m*sum(innerprod(idx)) + param1).^param2;
                clear innerprod idx m
            end
            
        otherwise
            error('Unknown kernel function.');
    end
end