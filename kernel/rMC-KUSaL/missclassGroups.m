function [miss,index] = missclassGroups(Segmentation,RefSegmentation,ngroups)

Permutations = perms(1:ngroups);
if(size(Segmentation,2)==1)
    Segmentation=Segmentation';
end
miss = zeros(size(Permutations,1),size(Segmentation,1));
for k=1:size(Segmentation,1)
    for j=1:size(Permutations,1)
        miss(j,k) = sum(Segmentation(k,:)~=Permutations(j,RefSegmentation));
    end
end

[miss,temp] = min(miss,[],1);
index = Permutations(temp,:);
