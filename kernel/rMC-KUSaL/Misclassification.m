function Missrate = Misclassification(groups,s)

n = max(s);
for i = 1:size(groups,2)
    Missrate(i,1) = missclassGroups( groups(:,i),s,n ) ./ length(s); 
end