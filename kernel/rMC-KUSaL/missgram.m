function G = missgram(missX1, missX2, kernel, param1, param2)

    % Check inputs
    if size(missX1,1) ~= size(missX2,1)
        error('Dimensionality of both datasets should be equal');
    end

    % If no kernel function is specified
    if nargin == 2 || strcmp(kernel, 'none')
        kernel = 'linear';
    end
    
    n = size(missX1,1);
    G = zeros(size(missX1,2),size(missX2,2));
    
    switch kernel
        
        % Linear kernel
        case 'linear'
            for i = 1:size(missX1,2)
                for j = 1:size(missX2,2)
                    innerprod = missX1(:,i).*missX2(:,j);
                    idx = find(~isnan(innerprod));
                    m = length(idx);
                    G(i,j) = n/m*sum(innerprod(idx));
                    clear innerprod idx m
                end
            end
        
        % Gaussian kernel
        case 'gauss'
            if ~exist('param1', 'var'), param1 = 1; end
            for i = 1:size(missX1,2)
                for j = 1:size(missX2,2)
                    minus = missX1(:,i)-missX2(:,j);
                    idx = find(~isnan(minus));
                    m = length(idx);
                    G(i,j) = n/m*sum(minus(idx).^2);
                    clear minus idx m
                end
            end
            G = exp(-(G./param1));
                        
        % Polynomial kernel
        case 'poly'
            if ~exist('param1', 'var'), param1 = 1; param2 = 3; end
            for i = 1:size(missX1,2)
                for j = 1:size(missX2,2)
                    innerprod = missX1(:,i).*missX2(:,j);
                    idx = find(~isnan(innerprod));
                    m = length(idx);
                    G(i,j) = ( n/m*sum(innerprod(idx)) + param1).^param2;
                    clear innerprod idx m
                end
            end
            
        otherwise
            error('Unknown kernel function.');
    end
end