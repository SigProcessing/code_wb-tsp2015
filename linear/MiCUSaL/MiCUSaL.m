function D = MiCUSaL( Y, s, L, Dinit, lambda, maxIter )

% INPUT:
% Y: training data, every column in Y corresponds to one sample
% s: dimension of the subspaces
% L: number of subspaces
% Dinit: initial D
% lambda: regularization parameter
% maxIter: maximum number of iterations
%
% OUTPUT:
% D: final orthonormal bases, every block corresponds to one subspace

D = Dinit;
for iter = 1:maxIter
    subidx = SubspaceAssign(D, Y, s, L);
    D = SubspaceUpdate(Y, D, subidx, s, L, lambda);
end

end


function D = SubspaceUpdate(Data, D, subidx, s, L, lambda)

for i = 1:L
    startidx = (i-1)*s+1;
    relevantDataIndices = find(subidx==i);
    if (length(relevantDataIndices)<1)
        betterD = D(:,startidx:startidx+s-1);
    else
        subData = Data(:,relevantDataIndices);
        betterD = FindBetterD(subData,D,startidx,s,L,lambda);
    end
    D(:,startidx:startidx+s-1) = betterD;
end

end


function betterD = FindBetterD(DatainSub, D, startidx, s, L, lambda)

D0idx = setdiff(1:size(D,2),startidx:startidx+s-1);
D0 = D(:,D0idx);
S = zeros(size(D,1));
for i = 1:L-1
    Di = D0(:,(i-1)*s+1:i*s);
    S = S + Di*Di';
end
S = S + lambda/2*DatainSub*DatainSub';

[U,Sigma] = eig(S);
Sigma = diag(Sigma);
[~,ix] = sort(Sigma,'descend');
U = U(:,ix);
betterD = U(:,1:s);

end


function subidx = SubspaceAssign(D, Y, s, L)

coher = zeros(L,size(Y,2));
for i = 1:L
    tempD = D(:,(i-1)*s+1:i*s);
    tempproj = tempD'*Y;
    coher(i,:) = sum(tempproj.^2,1);
    clear tempproj
end
[~, subidx] = max(coher,[],1);

end