function [newD, estidim] = SubspaceDimEstimation(DenoisedData, D, subidx, L, k1, k2)

olddim = size(D,2)/L;
dimes = zeros(L,1);

for i = 1:L
    relevantDataIndices = find(subidx==i);
    subData = DenoisedData(:,relevantDataIndices);
    dimes(i) = round(mledim(subData,k1,k2));
end

estidim = max(dimes);
newD = zeros(size(D,1),estidim*L);
for i = 1:L
    newD(:,(i-1)*estidim+1:i*estidim) = D(:,(i-1)*olddim+1:(i-1)*olddim+estidim);
end

end

