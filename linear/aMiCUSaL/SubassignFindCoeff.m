function [Coeff, subidx] = SubassignFindCoeff(D, Y, s, L)

Coeff = zeros(size(D,2),size(Y,2));
coher = zeros(L,size(Y,2));
for i = 1:L
    tempD = D(:,(i-1)*s+1:i*s);
    tempproj = tempD'*Y;
    coher(i,:) = sum(tempproj.^2,1);
    clear tempproj
end
[~, subidx] = max(coher,[],1);

for j = 1:size(Y,2)
    Phi = D(:,(subidx(j)-1)*s+1:subidx(j)*s);
    Coeff((subidx(j)-1)*s+1:subidx(j)*s,j) = Phi'*Y(:,j);
    clear Phi
end

end