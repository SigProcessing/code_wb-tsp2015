function D = ksubmiss( missY, s, L, Dinit, stepsize, maxCycles, maxIter )

% INPUT:
% Y: training data with missing entries, every column in Y corresponds to one sample
% s: dimension of the subspaces
% L: number of subspaces
% Dinit: initial D
% stepsize: the parameter for stochastic gradient descent step size
% maxCycles: number of passes over the data in each iteration
% maxIter: maximum number of iterations
%
% OUTPUT:
% D: final orthonormal bases, every block corresponds to one subspace

D = Dinit;
for iter = 1:maxIter
    subidx = MissingSubspaceAssign(D, missY, s, L);
    D = SubUpdateGROUSE(missY, D, subidx, L, s, stepsize, maxCycles);
end


end


function [Dict] = SubUpdateGROUSE(IncompleteData, Dict, subidx, nSubs, dim, step_size, maxCycles)

for j = 1:nSubs
    bstartidx = (j-1)*dim+1;
    relevantDataIndices = find(subidx==j);
    if (length(relevantDataIndices)<1)
        betterDictionaryElements = Dict(:,bstartidx:bstartidx+dim-1);
    else
        
        Y = IncompleteData(:,relevantDataIndices);
        U = Dict(:,bstartidx:bstartidx+dim-1);
        betterDictionaryElements = mygrouse(Y,step_size,maxCycles,U);
        clear U Y
    end
    Dict(:,bstartidx:bstartidx+dim-1) = betterDictionaryElements;
end

end


function [U] = mygrouse(Data, step_size, maxCycles, Uinit)
%  Inputs:
%       Data = incomplete data (with 0 at the non-observed position)
%       coeff = coefficients
%       dim = the dimension of the subspace
%       step_size = the constant for stochastic gradient descent step size
%       maxCycles = number of passes over the data
%       Uinit = an initial guess for the column space U
%   Outputs:
%       U = optimized orthonormal basis
%

Indicator = ~isnan(Data);
% Indicator = (Data~=0);

U = Uinit;

for outiter = 1:maxCycles
    
    oldU = U;
    
    for k = 1:size(Data,2)
        
        % Pull out the relevant indices and revealed entries for this column
        sigidx = find(Indicator(:,k));
        v_Omega = Data(sigidx,k);
        U_Omega = U(sigidx,:);
        
        
        weights = U_Omega\v_Omega;
        norm_weights = norm(weights);
        residual = v_Omega - U_Omega*weights;
        norm_residual = norm(residual);
        expandedresidual = zeros(size(U,1),1);
        expandedresidual(sigidx) = residual;
        
        sG = 2*norm_residual*norm_weights;
        
        t = step_size*sG/outiter;
        
        % Take the gradient step.
        if t<pi/2 % drop big steps
            alpha = (cos(t)-1)/norm_weights^2;
            beta = sin(t)/sG;
            
            U = U + U*(alpha*weights)*weights' + 2*beta*expandedresidual*weights';
        end
        clear expandedresidual sigidx v_Omega U_Omega weights residual sG t alpha beta norm_residual norm_weights
    end
   
    a = norm(oldU*oldU'-U*U','fro');
    if (a<=1e-6) break; end;
end

% fprintf('finished incremental gradient %d...\n',a);


end




function subidx = MissingSubspaceAssign(D, missY, s, L)

r = zeros(L,size(missY,2));
for i = 1:L
    for j = 1:size(missY,2)
        idx = find(~isnan(missY(:,j)));
        tempD = D(idx,(i-1)*s+1:i*s);
        w = tempD\missY(idx,j);
        r(i,j) = sum((missY(idx,j) - tempD*w).^2);
        clear idx tempD w
    end
end
[~, subidx] = min(r,[],1);

end